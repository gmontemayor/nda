/*
 * Copyright 2021 Gerardo Montemayor. All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.adn.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.JsonParseException;
import javax.inject.Inject;

import com.adn.solver.DNA;
import com.adn.exception.DNAException;
import com.adn.model.request.HasMutationRequest;
import com.adn.constants.ResponseCodes;
import com.adn.model.response.ErrorMessage;
import com.adn.exception.CouldNotSaveDnaException;
import com.adn.config.DnaComponent;
import com.adn.config.DaggerDnaComponent;
import com.adn.dao.DnaDao;
import com.adn.model.Dna;



/**
 * Handler for requests to Lambda function.
 */
public class DNAMutationHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    
    private final Gson gson = new Gson();
    

    @Inject
    DnaDao dnaDao;
    private final DnaComponent dnaComponent;

    public DNAMutationHandler() {
        dnaComponent = DaggerDnaComponent.builder().build();
        dnaComponent.inject(this);
    }



    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        //TODO: in TestContext class, check how to inject a valid logger to make context.getLogger() work in testing
        //context.getLogger().log("incoming event data " + event.toString());
        
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Custom-Header", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent().withHeaders(headers);

        HasMutationRequest request;
        try{
            request = gson.fromJson(event.getBody(), HasMutationRequest.class);
        }catch (JsonSyntaxException e) {
            response.setStatusCode(400);
            response.setBody(gson.toJson(new ErrorMessage("Bad request, syntax error", ResponseCodes.RC_BAD_REQUEST)));
            return response;          
        }catch (JsonParseException e) {
            response.setStatusCode(400);
            response.setBody(gson.toJson(new ErrorMessage("Bad request, parsing error", ResponseCodes.RC_BAD_REQUEST)));
            return response;          
        }

        if (request == null || request.getDna()==null) {
            response.setStatusCode(400);
            response.setBody(gson.toJson(new ErrorMessage("Bad request", ResponseCodes.RC_BAD_REQUEST)));
            return response;
        }

        try {
            if(DNA.hasMutation(request.getDna())){
                final Dna dna = dnaDao.saveDna(request, true);
                response.setStatusCode(200);
                return response;
            }else{
                final Dna dna = dnaDao.saveDna(request, false);
                response.setStatusCode(403);
                response.setBody(gson.toJson(new ErrorMessage("No mutation found", ResponseCodes.RC_NO_MUTATION)));
                return response;
            }
        } catch (DNAException e) {
            response.setStatusCode(403);
            response.setBody(gson.toJson(new ErrorMessage(e.getMessage(), e.getCode())));
            return response;
        } catch (CouldNotSaveDnaException e) {
            response.setStatusCode(403);
            response.setBody(gson.toJson(new ErrorMessage("Could not save dna", ResponseCodes.RC_COULD_NOT_SAVE_DNA)));
            return response;
        }
    }
}
