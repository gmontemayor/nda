/*
 * Copyright 2021 Gerardo Montemayor. All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.adn.dao;

import com.adn.exception.CouldNotSaveDnaException;
import com.adn.exception.TableDoesNotExistException;

import com.adn.model.Dna;

import com.adn.model.request.HasMutationRequest;

import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.ConditionalCheckFailedException;

import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ResourceNotFoundException;
import software.amazon.awssdk.services.dynamodb.model.PutItemResponse;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DnaDao {
    
    private static final String DNA_ID = "dnaId";
    private static final String DNA_HAS_MUTATION = "hasMutation";
    
    private final String tableName;
    private final DynamoDbClient dynamoDb;
    

    /**
     * Constructs an DnaDao.
     * @param dynamoDb dynamodb client
     * @param tableName name of table to use for dnas
     */
    public DnaDao(final DynamoDbClient dynamoDb, final String tableName) {
        this.dynamoDb = dynamoDb;
        this.tableName = tableName;
    }
    

    private Map<String, AttributeValue> createDnaItem(final HasMutationRequest request, final Boolean hasMutation) {
        Map<String, AttributeValue> item = new HashMap<>();
        item.put(DNA_ID, AttributeValue.builder().s(validateDna(request.getDna())).build());
        item.put(DNA_HAS_MUTATION, AttributeValue.builder().bool(hasMutation).build());

        return item;
    }

    private String validateDna(final String[] dna) {
        if (dna==null || dna.length<1) {
            throw new IllegalArgumentException("dna is null or empty");
        }
        return String.join("", dna);
    }

    /**
     * Saves a dna.
     * @param saveDnaRequest details of dna to save
     * @return saved dna
     */
    public Dna saveDna(final HasMutationRequest saveDnaRequest, final Boolean hasMutation) {
        if (saveDnaRequest == null) {
            throw new IllegalArgumentException("saveDnaRequest was null");
        }
        int tries = 0;
        while (tries < 10) {
            try {
                Map<String, AttributeValue> item = createDnaItem(saveDnaRequest, hasMutation);
                PutItemResponse response = dynamoDb.putItem(PutItemRequest.builder()
                        .tableName(tableName)
                        .item(item)
                        .conditionExpression("attribute_not_exists(dna)")
                        .build());

                return Dna.builder()
                        .dnaId(item.get(DNA_ID).s())
                        .hasMutation(item.get(DNA_HAS_MUTATION).bool())
                        .build();
            } catch (ConditionalCheckFailedException e) {
                tries++;
            } catch (ResourceNotFoundException e) {
                throw new TableDoesNotExistException( "Dna table " + tableName + " does not exist");
            }
        }
        throw new CouldNotSaveDnaException(
                "Unable to generate unique dna id after 10 tries");
    }

}

