/*
 * Copyright 2021 Gerardo Montemayor. All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.adn.solver;

import java.util.Vector;
import com.adn.exception.DNAException;
import com.adn.constants.ResponseCodes;

public class DNA {

	public DNA(){
		
	}

	/**
     * @return boolean
     * There is a mutation if MORE than one sequence of 4 identical letters is found; 
     * horizontally, vertically or diagonally (left and right).
     * 
     * Algorithm
     *
     * Pass once trough every position in the matrix and for each one store the 
     * cumulative sum Σ of its neighbours horizontally, vertically and diagonally (left and right).
     *
     * For example
     *
     * A T G C C
     * C A A C T
     * G T A C C
     * A C G A C
     * G T C C A 
     *
     * This DNA has a final matrix state like
     *
     * A{v=1, h=1, dl=1, dr=1} T{v=1, h=1, dl=1, dr=1} G{v=1, h=1, dl=1, dr=1} C{v=1, h=1, dl=1, dr=1} C{v=1, h=2, dl=1, dr=1} 
     * C{v=1, h=1, dl=1, dr=1} A{v=1, h=1, dl=2, dr=1} A{v=1, h=2, dl=1, dr=1} C{v=2, h=1, dl=1, dr=2} T{v=1, h=1, dl=1, dr=1} 
     * G{v=1, h=1, dl=1, dr=1} T{v=1, h=1, dl=1, dr=1} A{v=2, h=1, dl=3, dr=1} C{v=3, h=1, dl=1, dr=1} C{v=1, h=2, dl=2, dr=1} 
     * A{v=1, h=1, dl=1, dr=1} C{v=1, h=1, dl=1, dr=1} G{v=1, h=1, dl=1, dr=1} A{v=1, h=1, dl=4, dr=1} C{v=2, h=1, dl=2, dr=1} 
     * G{v=1, h=1, dl=1, dr=1} T{v=1, h=1, dl=1, dr=1} C{v=1, h=1, dl=2, dr=1} C{v=1, h=2, dl=1, dr=2} A{v=1, h=1, dl=5, dr=1}
     *
     * This example has 1 sequence of 5 chained A's in diagonal. It is not a mutation due
     * we need at least two sequences
     *
  	 */
	public static boolean hasMutation(String[] chain) throws DNAException{
		
		//printChain(chain);
        
		Vector<Vector<PositionState>> stateMatrix = createStateMatrix(chain);
		
		int numberOfSequencesOfFour = 0;

		outerloop:
		for(int i=0;i<chain.length;++i){
			for(int j=0;j<chain[i].length();++j){
				
				//Add 1 to left diagonal if letter matches
				if(i-1>=0 && j-1>=0 && chain[i].charAt(j)==stateMatrix.get(i-1).get(j-1).getLetter()){
					stateMatrix.get(i).get(j).setDiagonalLeft(stateMatrix.get(i-1).get(j-1).getDiagonalLeft()+1);

					//If there are exactly 4 letters chained in left diagonal, then a sequence was found
					if(stateMatrix.get(i).get(j).getDiagonalLeft()==4){
						numberOfSequencesOfFour+=1;
					}					
				}

				//Add 1 to right diagonal if letter matches
				if(i-1>=0 && j+1<chain.length && chain[i].charAt(j)==stateMatrix.get(i-1).get(j+1).getLetter()){
					stateMatrix.get(i).get(j).setDiagonalRight(stateMatrix.get(i-1).get(j+1).getDiagonalRight()+1);

					//If there are exactly 4 letters chained in right diagonal, then a sequence was found
					if(stateMatrix.get(i).get(j).getDiagonalRight()==4){
						numberOfSequencesOfFour+=1;
					}					
				}

				//Add 1 to vertical if letter matches
				if(i-1>=0 && chain[i].charAt(j)==stateMatrix.get(i-1).get(j).getLetter()){
					stateMatrix.get(i).get(j).setVertical(stateMatrix.get(i-1).get(j).getVertical()+1);

					//If there are exactly 4 letters chained in vertical, then a sequence was found
					if(stateMatrix.get(i).get(j).getVertical()==4){
						numberOfSequencesOfFour+=1;
					}					
				}

				//Add 1 to horizontal if letter matches
				if(j-1>=0 && chain[i].charAt(j)==stateMatrix.get(i).get(j-1).getLetter()){
					stateMatrix.get(i).get(j).setHorizontal(stateMatrix.get(i).get(j-1).getHorizontal()+1);

					//If there are exactly 4 letters chained in horizontal, then a sequence was found
					if(stateMatrix.get(i).get(j).getHorizontal()==4){
						numberOfSequencesOfFour+=1;
					}					
				}

				if(numberOfSequencesOfFour>=2){
					break outerloop;
				}
			}
		}
	    
	    //printStateMatrix(stateMatrix);

		return numberOfSequencesOfFour>=2;
	}


	/**
     * @return Vector< Vector<PositionState> >
     * Creates an empty state matrix
     */
    private static Vector<Vector<PositionState>> createStateMatrix(String[] chain) throws DNAException{
        Vector<Vector<PositionState>> matrix = new Vector<Vector<PositionState>>();
        for (int i = 0; i < chain.length; ++i) {
        	if(chain.length != chain[i].length()){
        		throw new DNAException(ResponseCodes.RC_BAD_SIZE, "Error, DNA is not a square matrix");
        	}
        	Vector row = new Vector<PositionState>();
            for (int j = 0; j < chain[0].length(); ++j) {
            	if(chain[i].charAt(j)!='C' && chain[i].charAt(j)!='G' && chain[i].charAt(j)!='T' && chain[i].charAt(j)!='A'){
            		throw new DNAException(ResponseCodes.RC_BAD_CHARS, "Error, bad DNA chars "+chain[i].charAt(j));
            	}
                row.add(new PositionState(chain[i].charAt(j)));
            }
            matrix.add(row);
        }
        return matrix;
    }

 	/**
     * Prints state matrix
     */
    private static void printStateMatrix(Vector<Vector<PositionState>> stateMatrix) {
        for (int i = 0; i < stateMatrix.size(); ++i) {
            for (int j = 0; j < stateMatrix.get(0).size(); ++j) {
                System.out.print(stateMatrix.get(i).get(j) + "");
            }
            System.out.println();
        }
        System.out.println();
    }

	/**
     * Prints the DNA chain
     */
    private static void printChain(String[] chain) {
		for(int i=0;i<chain.length;++i){
			for(int j=0;j<chain[i].length();++j){
				System.out.print(chain[i].charAt(j));
			}
			System.out.println();
		}
    }

}

/**
 * State of a letter in the matrix. Saving the cumulative sum Σ of its neighbours
 * horizontally, vertically and diagonally (left and right)
 */
class PositionState{
	private int vertical=1;
	private int horizontal=1;
	private int diagonalLeft=1;
	private int diagonalRight=1;
	private char letter;
	
	PositionState(char letter){
		this.letter = letter;
	}

	public char getLetter(){
		return letter;
	}

	public int getVertical(){
		return vertical;
	}

	public void setVertical(int vertical){
		this.vertical=vertical;
	}

	public int getHorizontal(){
		return horizontal;
	}

	public void setHorizontal(int horizontal){
		this.horizontal=horizontal;
	}

	public int getDiagonalLeft(){
		return diagonalLeft;
	}

	public void setDiagonalLeft(int diagonalLeft){
		this.diagonalLeft=diagonalLeft;
	}

	public int getDiagonalRight(){
		return diagonalRight;
	}

	public void setDiagonalRight(int diagonalRight){
		this.diagonalRight=diagonalRight;
	}

	public String toString(){
		return letter+"{v="+vertical+", h="+horizontal+", dl="+diagonalLeft+", dr="+diagonalRight+"} ";
	}

}
