package com.adn.solver;

public class Main {
    public static void main(String[] args) throws Exception{
        
        boolean hasMutation = DNA.hasMutation(new String[]{ 
            "AAGCGA", 
            "CAGTGC", 
            "TTATGT", 
            "AGAAGG", 
            "CCCCTA",
            "TCACTG" 
        });
        System.out.println("has mutation " + hasMutation);


        hasMutation = DNA.hasMutation(new String[]{ 
            "AAGCGA", 
            "AAGTGC", 
            "TTATGT", 
            "AGGGGC", 
            "ACCCTA",
            "TCACTG" 
        });
        System.out.println("has mutation " + hasMutation);
        
        hasMutation = DNA.hasMutation(new String[]{ 
            "ATGCC",
            "CAACT",
            "GTACC",
            "ACGAC",
            "GTCCA"
        });
        System.out.println("has mutation " + hasMutation);
        

    }
}
