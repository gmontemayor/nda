/*
 * Copyright 2021 Gerardo Montemayor. All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.adn.solver;

import org.junit.jupiter.api.Test;
import java.io.IOException;
import com.adn.exception.DNAException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HasMutationTest {
    @Test
    public void hasMutation_whenDNAEmpty_returnsFalse() throws IOException, DNAException {
        assertFalse(DNA.hasMutation(new String[]{ }));
    }

    @Test
    public void hasMutation_whenHasMutation_returnsTrue() throws IOException, DNAException {
        assertTrue(DNA.hasMutation(new String[]{ 
            "AAGCGA", 
            "CAGTGC", 
            "TTATGT", 
            "AGAAGG", 
            "CCCCTA",
            "TCACTG" 
            })
        );
    }

    @Test
    public void hasMutation_whenHasNoMutation_returnsFalse() throws IOException, DNAException {
        assertFalse(DNA.hasMutation(new String[]{ 
            "ATGCC",
            "CAACT",
            "GTACC",
            "ACGAC",
            "GTCCA"
            })
        );
    }

    @Test
    public void hasMutation_whenDNAHasWrongChars_ExceptionThrownWithCode1000() throws IOException {
        DNAException exception = assertThrows(DNAException.class, () -> {
            DNA.hasMutation(new String[]{ 
                "AAGCGA", 
                "XXXXXX", 
                "TTXTGT", 
                "AGAXGG", 
                "CCCCXA",
                "TCACTX" 
            });
        });
        assertTrue(exception.getCode().equals("1000"));
    }

    @Test
    public void hasMutation_whenDNAIsNotASquareMatrix_ExceptionThrownWithCode1001() throws IOException {
        DNAException exception = assertThrows(DNAException.class, () -> {
            DNA.hasMutation(new String[]{ 
                "AAGCGA", 
                "CGTACGTA", 
                "TTATGT", 
                "AGAAGG", 
                "CCCCAA",
                "TCACTA" 
            });
        });

        assertTrue(exception.getCode().equals("1001"));
    }
}
