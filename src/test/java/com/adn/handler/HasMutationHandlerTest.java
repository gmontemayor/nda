/*
 * Copyright 2021 Gerardo Montemayor. All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.adn.handler;

import org.junit.jupiter.api.Test;
import java.io.IOException;
import com.adn.exception.DNAException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.adn.services.lambda.runtime.TestContext;

import com.amazonaws.services.lambda.runtime.Context;

public class HasMutationHandlerTest {
    private DNAMutationHandler sut = new DNAMutationHandler();

    @Test
    public void handleRequest_whenDNAMutationInputStreamHasNoMutation_puts403InResponse() throws IOException {
        APIGatewayProxyRequestEvent input = new APIGatewayProxyRequestEvent();
        input.setBody("{\"dna\":  [\"TTGCGA\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"GCCCTA\", \"TCACTG\"]}");
        APIGatewayProxyResponseEvent response = sut.handleRequest(input, TestContext.builder().build());
        assertTrue(response.toString().contains("statusCode: 403"));
    }

    @Test
    public void handleRequest_whenDNAMutationInputStreamHasMutation_puts200InResponse() throws IOException {
        APIGatewayProxyRequestEvent input = new APIGatewayProxyRequestEvent();
        input.setBody("{\"dna\":  [\"ATGCGA\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"]}");
        APIGatewayProxyResponseEvent response = sut.handleRequest(input, TestContext.builder().build());
        assertTrue(response.toString().contains("statusCode: 200"));
    }

    @Test
    public void handleRequest_whenDNAMutationInputStreamEmpty_puts400InResponse() throws IOException {
        APIGatewayProxyRequestEvent input = new APIGatewayProxyRequestEvent();
        
        APIGatewayProxyResponseEvent response = sut.handleRequest(input, TestContext.builder().build());
        assertTrue(response.toString().contains("statusCode: 400"));
        assertTrue(response.toString().contains("\"code\":\"1003\""));
    }

    @Test
    public void handleRequest_whenDNAMutationInputStreamHasNoBody_puts400InResponse() throws IOException {
        APIGatewayProxyRequestEvent input = new APIGatewayProxyRequestEvent();
        input.setBody("");
        APIGatewayProxyResponseEvent response = sut.handleRequest(input, TestContext.builder().build());
        assertTrue(response.toString().contains("statusCode: 400"));
        assertTrue(response.toString().contains("\"code\":\"1003\""));
    }

    @Test
    public void handleRequest_whenDNAMutationInputStreamHasEmptyBodyDict_puts400InResponse() throws IOException {
        APIGatewayProxyRequestEvent input = new APIGatewayProxyRequestEvent();
        input.setBody("{}");
        APIGatewayProxyResponseEvent response = sut.handleRequest(input, TestContext.builder().build());
        assertTrue(response.toString().contains("statusCode: 400"));
        assertTrue(response.toString().contains("\"code\":\"1003\""));
    }

    @Test
    public void handleRequest_whenDNAMutationInputStreamHasNullBody_puts400InResponse() throws IOException {
        APIGatewayProxyRequestEvent input = new APIGatewayProxyRequestEvent();
        input.setBody("{\"dna\": \"null\"}");
        APIGatewayProxyResponseEvent response = sut.handleRequest(input, TestContext.builder().build());
        assertTrue(response.toString().contains("statusCode: 400"));
        assertTrue(response.toString().contains("\"code\":\"1003\""));
    }

    @Test
    public void handleRequest_whenDNAMutationInputStreamHasWrongTypeForBody_puts400InResponse() throws IOException {
        APIGatewayProxyRequestEvent input = new APIGatewayProxyRequestEvent();
        input.setBody("{\"dna\": \"1\"}");
        APIGatewayProxyResponseEvent response = sut.handleRequest(input, TestContext.builder().build());
        assertTrue(response.toString().contains("statusCode: 400"));
        assertTrue(response.toString().contains("\"code\":\"1003\""));
    }

    @Test
    public void handleRequest_whenDNAMutationInputStreamHasDifferentParams_puts400InResponse() throws IOException {
        APIGatewayProxyRequestEvent input = new APIGatewayProxyRequestEvent();
        input.setBody("{\"other\":  [\"ATGCGA\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"]}");
        APIGatewayProxyResponseEvent response = sut.handleRequest(input, TestContext.builder().build());
        assertTrue(response.toString().contains("statusCode: 400"));
        assertTrue(response.toString().contains("\"code\":\"1003\""));
    }


}
