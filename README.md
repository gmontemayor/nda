# Template for AWS Serverless Lambda Functions using SAM

Check if a DNA chain has mutation

## Description

Project to detect if a DNA has a mutation. The DNA is represented with a table of NxN with letters `C`, `G`, `T`, and `A`.
```bash
String[]{ 
	"AAGCGA", 
	"CAGTGC", 
	"TTATGT", 
	"AGAAGG", 
	"CCCCTA", 
	"TCACTG" 
} 
```
There is a mutation in the DNA if MORE than one sequence of 4 identical letters is found; horizontally, vertically or diagonally (left and right).

### Algorithm 

 Pass once trough every position in the matrix and for each one store the cumulative sum Σ of its neighbours horizontally, vertically and diagonally (left and right). For example

```bash     
      A T G C C
      C A A C T
      G T A C C
      A C G A C
      G T C C A 
```
This DNA has a final matrix state like

```bash  
      A{v=1, h=1, dl=1, dr=1} T{v=1, h=1, dl=1, dr=1} G{v=1, h=1, dl=1, dr=1} C{v=1, h=1, dl=1, dr=1} C{v=1, h=2, dl=1, dr=1} 
      C{v=1, h=1, dl=1, dr=1} A{v=1, h=1, dl=2, dr=1} A{v=1, h=2, dl=1, dr=1} C{v=2, h=1, dl=1, dr=2} T{v=1, h=1, dl=1, dr=1} 
      G{v=1, h=1, dl=1, dr=1} T{v=1, h=1, dl=1, dr=1} A{v=2, h=1, dl=3, dr=1} C{v=3, h=1, dl=1, dr=1} C{v=1, h=2, dl=2, dr=1} 
      A{v=1, h=1, dl=1, dr=1} C{v=1, h=1, dl=1, dr=1} G{v=1, h=1, dl=1, dr=1} A{v=1, h=1, dl=4, dr=1} C{v=2, h=1, dl=2, dr=1} 
      G{v=1, h=1, dl=1, dr=1} T{v=1, h=1, dl=1, dr=1} C{v=1, h=1, dl=2, dr=1} C{v=1, h=2, dl=1, dr=2} A{v=1, h=1, dl=5, dr=1}
```     
This example has 1 sequence of 5 chained A's in diagonal. It is not a mutation due we need at least two sequences

## Dependencies
* [Install and configure AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
* [Java SE Development Kit 8 installed](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Docker installed](https://www.docker.com/community-edition)
* [Maven](https://maven.apache.org/install.html)
* [SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
* [Python 3](https://docs.python.org/3/)

## Setup process

* Create a user in AWS IAM and assign permissions with a custom policy
* Using IAM credentials (AccessKey and SecretKey) configure an AWS profile (choose the correct AWS region)
```bash
aws configure --profile slsDev
```

### Local setup development

1. Start DynamoDB Local in a Docker container. `docker run -p 8000:8000 amazon/dynamodb-local`
2. Create the DynamoDB table. `aws dynamodb create-table --table-name dna_table --region us-east-2 --attribute-definitions AttributeName=dnaId,AttributeType=S --key-schema AttributeName=dnaId,KeyType=HASH --billing-mode PAY_PER_REQUEST --endpoint-url http://localhost:8000`


### Installing dependencies

`maven` is used to install dependencies and package the application into a JAR file:

```bash
mvn clean package
```

## Testing

### Running unit tests
`JUnit` is used for testing.
Use the following command:

```bash
mvn test
```

## Running local API

To run our API in local, run 

```bash
sam local start-api
```

Now, you can use the endpoint `http://127.0.0.1:3000/mutation`. Remember to set properly the body of the `POST` request in the `json` format. It should be like
```bash
{
  "dna":["CGTAGA", "CAGTGC", "TTATGT", "AGATGG", "GCCCTA", "TCACTG"]
}
```

### Setup your s3 to upload the package
If you still don't have a S3 bucket to store the package then create one. 

### Package with SAM
```bash
sam package --s3-bucket s3-bucket-name --profile slsDev --region us-east-2 --output-template-file template-export.yml
```
where `s3-bucket-name` is the bucket name, `slsDev` is our AWS Profile name, `us-east-2` is our region and `template-export.yml` is our output yml definition file

### Deploy with SAM

```bash
sam deploy --template-file template-export.yml --profile slsDev --region us-east-2 --stack-name DNA --capabilities CAPABILITY_IAM
```

where `DNA` is our Cloudformation stack name. Now, copy the URL value with the form

```bash
https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod/
```

And now use the new URL adding the function name, like `https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod/mutation` and set the JSON request body

### Delete the aws stack

```bash
aws cloudformation delete-stack --profile slsDev --region us-east-2 --stack-name DNA
```
